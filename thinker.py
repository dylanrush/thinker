# Reviving Thinking Man using a raspberry pi zero w
# GPIO 21 is a transistor that opens the serial connection
import RPi.GPIO as GPIO
import time
import random
import requests

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(21, GPIO.OUT)

GPIO.output(21, 1)
time.sleep(1)
GPIO.output(21, 0)
time.sleep(1)
GPIO.output(21, 1)

from Adafruit_Thermal import *

printer = Adafruit_Thermal("/dev/ttyS0", 19200, timeout=5)
printer.reset()
printer.online()

def get_thought():
    url = 'https://www.reddit.com/r/showerthoughts/hot.json'
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8', 'User-agent': 'thinking-man'}
    r = requests.get(url, headers=headers)
    postnum = random.choice(range(2,9))
    return r.json()['data']['children'][postnum]['data']['title']

while True:
    input_state = GPIO.input(18)
    if input_state == False:
        thought = get_thought()
        printer.reset()
	printer.feed(1)
        printer.println(thought)
        printer.feed(3)
        time.sleep(1)

