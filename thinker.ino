/* Reddit Shower Thoughts Printer
 * "Thinking Man"
 * By Dylan Rush 2015
 * Blog post: http://blog.dylanhrush.com/2015/03/thinking-man.html
 * Makezine.com article: http://makezine.com/projects/build-reddit-shower-thoughts-printer/
 * Source code repository: https://bitbucket.org/dylanrush/thinker/src/
 *
 * Instructions:
 * In order to get this to work, you'll have to set your WiFi SSID and
 * passcode in the constants below. Initially, the program will only
 * output post titles to USB serial. To enable the printer and disable
 * serial output, you will need to comment or uncomment lines below.
*/

#include "SoftwareSerial.h"
#include <Adafruit_Thermal.h>

// Set your WiFi ssid and passcode here.
#define WIFI_CONNECT "AT+CWJAP=\"YOUR_SSID\",\"YOUR_PASSCODE\""

// Uncomment SERIAL_USB and ENABLE_USB
// to see output using the Serial Monitor.
// I recommend starting with these enabled
// to verify that you can connect to the
// server. These should be commented out
// when using the thermal printer.
#define SERIAL_USB Serial
#define ENABLE_USB_DEBUG 1

// Uncomment ENABLE_PRINTER to enable the printer.
// I recommend starting with the printer disabled
// and debugging through USB to verify that the
// ESP-8266 can connect to Reddit.
#define ENABLE_PRINTER 1

// For extra debugging, uncomment to see which AT
// commands are sent to the ESP-8266 when USB
// output is enabled.
// #define ECHO_COMMANDS

#define DEST_IP     "www.reddit.com"
#define SERVER_CONNECT   "AT+CIPSTART=0,\"TCP\",\"wwww.reddit.com\",80"
#define GET         "GET /r/showerthoughts/hot.json HTTP/1.1\r\nHost: www.reddit.com\r\nAccept: application/json\r\n\r\n"
#define TIMEOUT     5000 // mS
// Will pick a random post title between 1 and MAX_POSTS
#define MAX_POSTS 24

// Halt program execution when connection fails
#define CONTINUE    false
#define HALT        true

#define BUFFER_SIZE 512

#define SERIAL_WIFI Serial1
#define PRINTER_TX 6  // This is the yellow wire
#define PRINTER_RX 5  // This is the green wire
#define BUTTON_PIN 7  // This is connected to the "New Thought" button
#define LED_PIN 13  // This is connected to the LED "Loading" indicator

SoftwareSerial mySerial(PRINTER_RX, PRINTER_TX); // Declare SoftwareSerial obj first

// title_start is the token the Arduino looks for
// to indicate the start of a post title.
static const String title_start("\"title\": \"");

#ifdef ENABLE_PRINTER
Adafruit_Thermal printer(&mySerial);
#endif

// msgBuffer is used to remember a post title and print it back
// with word wrap.
char msgBuffer[BUFFER_SIZE];
int msgBufferIndex = 0;

// Print error message and loop stop.
void wifi_errorHalt(String msg)
{
#ifdef ENABLE_USB_DEBUG
  SERIAL_USB.println(msg);
  SERIAL_USB.println("HALT");
#endif
  while (true) {};
}

// Read characters from WiFi module and echo to SERIAL_USB until keyword occurs or timeout.
boolean wifi_errorFind(String keyword)
{
  byte current_char   = 0;
  byte keyword_length = keyword.length();

  // Fail if the target string has not been sent by deadline.
  long deadline = millis() + TIMEOUT;
  while (millis() < deadline)
  {
    if (SERIAL_WIFI.available())
    {
      char ch = SERIAL_WIFI.read();
#ifdef ENABLE_USB_DEBUG
      SERIAL_USB.write(ch);
#endif
      if (ch == keyword[current_char])
        if (++current_char == keyword_length)
        {
#ifdef ENABLE_USB_DEBUG
          SERIAL_USB.println();
#endif
          return true;
        }
    }
  }
  return false;  // Timed out
}

// Read and echo all available module output.
// (Used when we're indifferent to "OK" vs. "no change" responses or to get around firmware bugs.)
void wifi_echoFlush()
{
  while (SERIAL_WIFI.available()) {
    char readChar = SERIAL_WIFI.read();
#ifdef ENABLE_USB_DEBUG
    SERIAL_USB.write(readChar);
#endif
  }
}

// Echo module output until 3 newlines encountered.
// (Used when we're indifferent to "OK" vs. "no change" responses.)
void wifi_echoSkip()
{
  wifi_errorFind("\n");        // Search for nl at end of command echo
  wifi_errorFind("\n");        // Search for 2nd nl at end of response.
  wifi_errorFind("\n");        // Search for 3rd nl at end of blank line.
}

// Send a command to the module and wait for acknowledgement string
// (or flush module output if no ack specified).
// Echoes all data received to the SERIAL_USB monitor.
boolean wifi_echoCommand(String cmd, String ack, boolean halt_on_fail)
{
  SERIAL_WIFI.println(cmd);
#ifdef ENABLE_USB_DEBUG
  SERIAL_USB.print("--"); SERIAL_USB.println(cmd);
#endif

  // If no ack response specified, skip all available module output.
  if (ack == "")
    wifi_echoSkip();
  else
    // Otherwise wait for ack.
    if (!wifi_errorFind(ack))          // timed out waiting for ack string
      if (halt_on_fail)
        wifi_errorHalt(cmd + " failed"); // Critical failure halt.
      else
        return false;            // Let the caller handle it.
  return true;                   // ack blank or ack found
}

// Connect to the specified wireless network.
boolean connectWiFi()
{
  if (wifi_echoCommand(WIFI_CONNECT, "OK", CONTINUE)) // Join Access Point
  {
#ifdef ENABLE_USB_DEBUG
    SERIAL_USB.println("Connected to WiFi.");
#endif
    return true;
  }
  else
  {
#ifdef ENABLE_USB_DEBUG
    SERIAL_USB.println("Connection to WiFi failed.");
#endif
    return false;
  }
}

// ******** SETUP ********
void setup() {
  randomSeed(analogRead(A0));
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  
#ifdef SERIAL_USB
  SERIAL_USB.begin(115200);         // Communication with PC monitor via USB
  SERIAL_USB.println("Thinking...");
#endif
  SERIAL_WIFI.begin(115200);        // Communication with ESP8266 via 5V/3.3V level shifter
  SERIAL_WIFI.setTimeout(TIMEOUT);

#ifdef ENABLE_PRINTER
  mySerial.begin(19200);
  printer.begin();
  //printer.println(F("Thinking..."));
#endif

  delay(2000);

  wifi_echoCommand("AT+RST", "ready", HALT);    // Reset & test if the module is ready
  wifi_echoCommand("AT+CWMODE=3", "", HALT);    // Station mode
  wifi_echoCommand("AT+CIPMUX=1", "", HALT);    // Allow multiple connections (we'll only use the first).

  //connect to the wifi
  boolean connection_established = false;
  for (int i = 0; i < 5; i++)
  {
    if (connectWiFi())
    {
      connection_established = true;
      break;
    }
  }
  if (!connection_established) wifi_errorHalt("Connection failed");

  delay(5000);
}

// Gets a character from the wifi controller
char getNextCharWaitAvailable() {
  while (!SERIAL_WIFI.available());
  return SERIAL_WIFI.read();
}

// Gets a character from the wifi controller, skipping
// markers for TCP packets.
char getNextCharSkipPacketMarkers() {
  char readChar = getNextCharWaitAvailable();
  if (readChar == '\r') {
    for (int i = 0; i < 19; i++) {
      getNextCharWaitAvailable();
    }
    readChar = getNextCharWaitAvailable();
  }
  return readChar;
}

// Keep reading page until title JSON marker is found
void eatTitle() {
  int title_start_search = 0;
  while (true) {
    char readChar = getNextCharSkipPacketMarkers();
#ifdef ENABLE_USB_DEBUG
    //SERIAL_USB.print(readChar);
#endif
    if (readChar == title_start[title_start_search]) {
      title_start_search ++;
      if (title_start_search == title_start.length())
      {
        break;
      }
    }
    else {
      title_start_search = 0;
    }
  }
}

// Writes a character to the message buffer
void writeChar(char toWrite) {
  if (msgBufferIndex < BUFFER_SIZE - 1)
    msgBuffer[msgBufferIndex++] = toWrite;
}

#ifdef ENABLE_PRINTER
// Prints the thought to the thermal printer,
// inserting line breaks at whitespace to avoid
// breaking words apart.
void printWordWrap(const char* str, int max_cols) {
  int col = 0;
  while (*str != '\0') {
    int next = nextWordLength(str);
    if (next == 0) {
      break;
    }
    if (col + next >= max_cols) {
      printer.println();
      for (; *str == ' ' || *str == '\n'; str++);
      col = 0;
    }
    while (*str != ' ' && *str != '\n' && *str != '\0') {
      printer.print(*str++);
      col++;
    }
    while (*str == ' ' || *str == '\n') {
      if (col < max_cols) {
        printer.print(*str++);
      }
      col++;
    }
    if (col >= max_cols) {
      col = 0;
    }
  }
}

int nextWordLength(const char* str) {
  int len = 0;
  for (; str[len] != ' ' && str[len] != '\n' && str[len] != '\0'; len++);
  return len;
}
#endif


// ******** LOOP ********
void loop()
{
  // Connect to the reddit.com server
  if (!wifi_echoCommand(SERVER_CONNECT, "OK", CONTINUE)) {
    delay(1000);
    return;
  }
  delay(2000);
  String cmd = GET;
  {
    String cmdLength = String(cmd.length());
    String atSend = "AT+CIPSEND=0,";
    if (!wifi_echoCommand(atSend + cmdLength, ">", CONTINUE))
    {
      wifi_echoCommand("AT+CIPCLOSE", "", CONTINUE);
      return;
    }
  }

  // Wait for "OK" response
  wifi_echoCommand(cmd, "OK", CONTINUE);

  // Select a random post #
  int post = 1 + random(0, MAX_POSTS);

#ifdef ENABLE_USB_DEBUG
  SERIAL_USB.print("Post#");
  SERIAL_USB.println(post);
#endif

  // Skip the first N posts
  for (int i = 0; i < post; i++) {
    eatTitle();
  }

#ifdef ENABLE_USB_DEBUG
  SERIAL_USB.println("(Start)");
#endif
  boolean escaped = false;

  // Read the post title into the message buffer
  char readChar;
  while (msgBufferIndex < BUFFER_SIZE - 1) {
    readChar = getNextCharSkipPacketMarkers();
    if (readChar == '"') {
      if (escaped) {
        writeChar(readChar);
      }
      else {
        break;
      }
    }
    escaped = readChar == '\\';
    if (!escaped && readChar != '"') {
      writeChar(readChar);
    }
  }
  msgBuffer[msgBufferIndex] = '\0';

  // Print the message
#ifdef ENABLE_PRINTER
  printWordWrap(msgBuffer, 32);
  printer.feed(4);
#endif
#ifdef SERIAL_USB
  SERIAL_USB.println(msgBuffer);
  SERIAL_USB.println();
#endif

  msgBufferIndex = 0;

  // Close the connection to the server
  wifi_echoCommand("AT+CIPCLOSE=0", "", CONTINUE);

  digitalWrite(LED_PIN, LOW);   // turn the LED on (HIGH is the voltage level)
  
  // Wait for the "Next Thought" button to be pressed
  while (digitalRead(BUTTON_PIN) == HIGH) {
    delay(1);
  }
  digitalWrite(LED_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
}

